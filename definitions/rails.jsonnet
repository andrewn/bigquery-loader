local standardFields = import 'standard-fields.libsonnet';

{
  id: 'rails',
  // IMPORTANT: we only load data from 14h00 to 15h00 each day as a sample of a huge dataset!
  location: 'gs://gitlab-gprd-logging-archive/gke/rails/dt=%(year)04d-%(month)02d-%(day)02d/%(year)04d%(month)02d%(day)02d14*',
  partitioningField: 'time',
  fields:
    [
      {
        name: 'time',
        type: 'TIMESTAMP',
        mode: 'REQUIRED',
        extractScalar: '$.time',
        parseTimestamp: '%FT%H:%M:%E*SZ',
      },
      {
        name: 'controller',
        type: 'STRING',
        mode: 'NULLABLE',
        extractScalar: '$.controller',
      },
      {
        name: 'action',
        type: 'STRING',
        mode: 'NULLABLE',
        extractScalar: '$.action',
      },
      {
        name: 'route',
        type: 'STRING',
        mode: 'NULLABLE',
        extractScalar: '$.route',
      },
      {
        name: 'meta_caller_id',
        type: 'STRING',
        mode: 'NULLABLE',
        extractScalar: "$['meta.caller_id']",
      },
      {
        name: 'feature_category',
        type: 'STRING',
        mode: 'NULLABLE',
        extractScalar: "$['meta.feature_category']",
      },
      {
        name: 'status',
        type: 'STRING',
        mode: 'NULLABLE',
        extractScalar: '$.status',
        safeCast: 'INT',
      },
      {
        name: 'shard',
        type: 'STRING',
        mode: 'NULLABLE',
        extractScalar: '$.shard',
      },
      {
        name: 'cpu_s',
        type: 'FLOAT',
        mode: 'NULLABLE',
        extractScalar: '$.cpu_s',
        safeCast: 'FLOAT64',
      },
      {
        name: 'meta_root_namespace',
        type: 'STRING',
        mode: 'NULLABLE',
        extractScalar: "$['meta.root_namespace']",
      },
      {
        name: 'meta_project',
        type: 'STRING',
        mode: 'NULLABLE',
        extractScalar: "$['meta.project']",
      },
      {
        name: 'duration_s',
        type: 'FLOAT',
        mode: 'NULLABLE',
        extractScalar: '$.duration_s',
        safeCast: 'FLOAT64',
      },
      {
        name: 'queue_duration_s',
        type: 'FLOAT',
        mode: 'NULLABLE',
        extractScalar: '$.queue_duration_s',
        safeCast: 'FLOAT64',
      },
      {
        name: 'external_http_count',
        type: 'INT64',
        mode: 'NULLABLE',
        extractScalar: "$.external_http_count",
        safeCast: 'INT64',
      },
      {
        name: 'external_http_duration_s',
        type: 'FLOAT',
        mode: 'NULLABLE',
        extractScalar: '$.external_http_duration_s',
        safeCast: 'FLOAT64',
      },
      {
        name: 'gitaly_calls',
        type: 'INT64',
        mode: 'NULLABLE',
        extractScalar: "$.gitaly_calls",
        safeCast: 'INT64',
      },
      {
        name: 'gitaly_duration_s',
        type: 'FLOAT',
        mode: 'NULLABLE',
        extractScalar: '$.gitaly_duration_s',
        safeCast: 'FLOAT64',
      }
    ] + standardFields.postgres()
}
