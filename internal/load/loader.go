package load

import (
	"context"
	"fmt"
	"time"

	"github.com/fatih/color"
	"github.com/google/go-jsonnet"
	log "github.com/sirupsen/logrus"
	"google.golang.org/api/iterator"

	"cloud.google.com/go/bigquery"
)

type loader struct {
	client            *bigquery.Client
	projectID         string
	datasetID         string
	tableID           string
	preloadTableID    string
	gcsBucket         string
	partitioningField string
	schema            bigquery.Schema
	transformQuery    string
	loadDate          time.Time
}

func makeJsonnetVM() (*jsonnet.VM, error) {
	vm := jsonnet.MakeVM()
	vm.Importer(&jsonnet.FileImporter{JPaths: []string{}})
	vm.ErrorFormatter.SetColorFormatter(color.New(color.FgRed).Fprintf)
	vm.StringOutput = true

	return vm, nil
}

func (c *loader) checkIfPartitionExists() (bool, error) {
	end := c.loadDate.AddDate(0, 0, 1)

	ctx := context.Background()

	sql := fmt.Sprintf(`
			SELECT EXISTS(
				SELECT 1
				FROM %s
				WHERE time >= @start AND time < @end
			) as partition_exists
		`, "`"+c.projectID+"."+c.datasetID+"."+c.tableID+"`")

	q := c.client.Query(sql)
	q.Parameters = []bigquery.QueryParameter{
		{Name: "start", Value: c.loadDate},
		{Name: "end", Value: end},
	}
	it, err := q.Read(ctx)
	if err != nil {
		return false, nil // Assume that a query error means that the table does not exist
	}
	var values []bigquery.Value
	err = it.Next(&values)
	if err == iterator.Done { // from "google.golang.org/api/iterator"
		return false, nil
	}
	if err != nil {
		return false, nil
	}
	fmt.Println(values)
	return values[0].(bool), nil
}

func (c *loader) importAsCSV() error {
	ctx := context.Background()

	gcsRef := bigquery.NewGCSReference(c.gcsBucket)
	gcsRef.SourceFormat = bigquery.CSV
	gcsRef.Schema = bigquery.Schema{
		{Name: "json", Type: bigquery.StringFieldType},
	}
	gcsRef.SkipLeadingRows = 0
	gcsRef.FieldDelimiter = "±"
	gcsRef.IgnoreUnknownValues = true
	gcsRef.MaxBadRecords = 100

	loader := c.client.Dataset(c.datasetID).Table(c.preloadTableID).LoaderFrom(gcsRef)
	loader.WriteDisposition = bigquery.WriteEmpty

	log.Infof("Submitting preload job to load %s:%s.%s", c.projectID, c.datasetID, c.preloadTableID)

	job, err := loader.Run(ctx)
	if err != nil {
		return err
	}

	log.WithField("job_id", job.ID()).Infof("Waiting for preload job to complete")

	status, err := job.Wait(ctx)
	if err != nil {
		return err
	}

	if status.Err() != nil {
		return fmt.Errorf("job completed with error: %v", status.Err())
	}
	return nil
}

func (c *loader) transformPreload() error {
	ctx := context.Background()

	table := c.client.Dataset(c.datasetID).Table(c.tableID)

	q := c.client.Query(c.transformQuery)
	q.Dst = table
	q.CreateDisposition = bigquery.CreateIfNeeded
	q.WriteDisposition = bigquery.WriteAppend
	q.TimePartitioning = &bigquery.TimePartitioning{
		Type:       bigquery.DayPartitioningType,
		Expiration: time.Duration(90 * 24 * time.Hour),
		Field:      c.partitioningField,
	}
	q.SchemaUpdateOptions = []string{"ALLOW_FIELD_ADDITION"}

	log.Infof("Submitting job to load %s:%s.%s", c.projectID, c.datasetID, c.tableID)

	job, err := q.Run(ctx)
	if err != nil {
		return err
	}

	log.WithField("job_id", job.ID()).Info("Waiting for job to complete")

	status, err := job.Wait(ctx)
	if err != nil {
		return err
	}

	if status.Err() != nil {
		return fmt.Errorf("job completed with error: %v", status.Err())
	}
	return nil
}

func (c *loader) deletePreloadTable() error {
	ctx := context.Background()

	table := c.client.Dataset(c.datasetID).Table(c.preloadTableID)
	if err := table.Delete(ctx); err != nil {
		return err
	}
	return nil
}

// func (c *loader) importJSONExplicitSchema(loadDate time.Time) error {
// 	ctx := context.Background()

// 	gcsRef := bigquery.NewGCSReference(c.gcsBucket)
// 	gcsRef.SourceFormat = bigquery.JSON
// 	gcsRef.Schema = c.schema
// 	gcsRef.IgnoreUnknownValues = true
// 	gcsRef.MaxBadRecords = 100

// 	loader := c.client.Dataset(c.datasetID).Table(c.tableID).LoaderFrom(gcsRef)
// 	loader.WriteDisposition = bigquery.WriteAppend
// 	loader.TimePartitioning = &bigquery.TimePartitioning{
// 		Type:       bigquery.DayPartitioningType,
// 		Expiration: time.Duration(90 * 24 * time.Hour),
// 		Field:      c.partitioningField,
// 	}

// 	log.Infof("Submitting job to load %s:%s.%s", c.projectID, c.datasetID, c.tableID)

// 	job, err := loader.Run(ctx)
// 	if err != nil {
// 		return err
// 	}

// 	log.WithField("job_id", job.ID()).Info("Waiting for job to complete")

// 	status, err := job.Wait(ctx)
// 	if err != nil {
// 		return err
// 	}

// 	if status.Err() != nil {
// 		return fmt.Errorf("job completed with error: %v", status.Err())
// 	}
// 	return nil
// }

func LoadLogDataForDate(definitionFile string, loadDate time.Time, datasetId string) error {
	vm, err := makeJsonnetVM()
	if err != nil {
		return err
	}

	loadDefinition, err := vm.EvaluateAnonymousSnippetMulti("import_fields", fmt.Sprintf(`
		local definition = import '%s';
		local loadDefinition = import 'definitions/load-definition.libsonnet';
		loadDefinition(definition, [%d,%d,%d], '%s')
	`, definitionFile, loadDate.Year(), loadDate.Month(), loadDate.Day(), datasetId))

	if err != nil {
		return err
	}

	schema, err := bigquery.SchemaFromJSON([]byte(loadDefinition["schema"]))
	if err != nil {
		return err
	}

	gcsBucket := loadDefinition["loadLocation"]
	projectID := loadDefinition["projectId"]
	datasetID := loadDefinition["datasetId"]
	tableID := loadDefinition["tableId"]
	transformQuery := loadDefinition["transformQuery"]
	partitioningField := loadDefinition["partitioningField"]
	preloadTableID := loadDefinition["preloadTableId"]

	ctx := context.Background()
	client, err := bigquery.NewClient(ctx, projectID)
	if err != nil {
		return fmt.Errorf("bigquery.NewClient: %v", err)
	}
	defer client.Close()

	c := &loader{
		loadDate:          loadDate,
		gcsBucket:         gcsBucket,
		projectID:         projectID,
		datasetID:         datasetID,
		tableID:           tableID,
		preloadTableID:    preloadTableID,
		partitioningField: partitioningField,
		schema:            schema,
		client:            client,
		transformQuery:    transformQuery,
	}

	exists, err := c.checkIfPartitionExists()
	if err != nil {
		return err
	}

	if exists {
		log.WithField("load_date", loadDate).Info("Skipping date as already loaded")
		return nil
	}

	err = c.importAsCSV()
	if err != nil {
		return err
	}

	err = c.transformPreload()
	if err != nil {
		return err
	}

	err = c.deletePreloadTable()
	if err != nil {
		return err
	}

	// err = c.importJSONExplicitSchema(loadDate)
	// if err != nil {
	// 	return err
	// }

	return nil
}
