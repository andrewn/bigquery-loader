package cmd

import (
	"log"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/gitlab-com/gl-infra/bigloader/internal/load"
)

var loadDate string
var loadDayCmd = &cobra.Command{
	Use:   "load-day",
	Short: "Load log data into bigquery",
	Long:  `Runs a job load`,
	Args:  cobra.ExactArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		layout := "2006-01-02"
		t, err := time.Parse(layout, loadDate)
		if err != nil {
			return err
		}

		if datasetId == "" {
			log.Fatal("Dataset is not set. Please set a valid dataset for gcloud.")
		}

		return load.LoadLogDataForDate(args[0], t, datasetId)
	},
}

func init() {
	rootCmd.AddCommand(loadDayCmd)

	loadDayCmd.PersistentFlags().StringVarP(&loadDate, "date", "d", "", "Date for load")

	err := loadDayCmd.MarkPersistentFlagRequired("date")
	if err != nil {
		log.Fatal(err)
	}

	loadDayCmd.PersistentFlags().StringVar(&datasetId, "dataset", "", "Dataset to load into")

	err = loadDayCmd.MarkPersistentFlagRequired("dataset")
	if err != nil {
		log.Fatal(err)
	}
}
